package psql_csv

import (
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	// TODO: Change to using pgx instead of database/sql and lib/pg
	// _ "github.com/jackc/pgx"
	_ "github.com/lib/pq"
)

// Tells whether a contains x.
func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// Creates a table from a provided CSV file, using the filename as the table name
func CreateTableFromCSV(csv_file string, db *sql.DB) {
	// The table name to use, derived from the filename
	var table_name string = strings.TrimSuffix(filepath.Base(csv_file), ".csv")
	// 2D slice of the data from the CSV file
	var rows [][]string = ReadCSV(csv_file)

	CreateTable(rows, table_name, db)
	InsertRowsToDatabase(rows, table_name, db)
}

// Reads in all rows from a CSV file and returns a 2D string slice of the data
func ReadCSV(file_path string) [][]string {
	log.Println("Opening CSV file: " + file_path)
	f, err := os.Open(file_path)
	if err != nil {
		log.Fatalf("Cannot open '%s': %s\n", file_path, err)
	}
	// defer f.Close()

	log.Println("Processing CSV file:", f.Name())
	r := csv.NewReader(f)
	// r.Comma = ';'
	r.LazyQuotes = true
	rows, err := r.ReadAll()
	if err != nil {
		log.Fatalln("Cannot read CSV data ("+file_path+"):", err)
	}

	return rows
}

// Creates SQL database table from 2D slice of strings
func CreateTable(rows [][]string, table_name string, db *sql.DB) {
	// header_str, _ := GetHeaderRow(rows)
	var data_type_sorted [][]string
	var column_data_types_slice []string
	var query_str string

	// Read all the rows, checking for type int, boolean, or string, sorted by column
	log.Println("Determining column data types")
	for i := range rows[0] {
		var data_type_line []string
		for _, data_entry := range rows[1:] {
			upper_trimmed := strings.ToUpper(strings.TrimSpace(data_entry[i]))
			if _, err := strconv.Atoi(data_entry[i]); err == nil {
				data_type_line = append(data_type_line, "int")
			} else if upper_trimmed == "TRUE" || upper_trimmed == "FALSE" || upper_trimmed == "T" || upper_trimmed == "F" {
				data_type_line = append(data_type_line, "bool")
			} else {
				data_type_line = append(data_type_line, "string")
			}
		}
		sort.Strings(data_type_line)
		data_type_sorted = append(data_type_sorted, data_type_line)
	}

	// Check the data type for the column
	for _, a := range data_type_sorted {
		var column_data_type string
		if Contains(a, "int") {
			column_data_type = "int"
		}
		if Contains(a, "bool") {
			column_data_type = "boolean"
		}
		if Contains(a, "string") {
			column_data_type = "text"
		}
		column_data_types_slice = append(column_data_types_slice, column_data_type)
	}

	log.Println("Constructing table creation query.")
	query_str = "CREATE TABLE IF NOT EXISTS " + table_name + "("
	for i, title := range rows[0] {
		if i != 0 {
			query_str = query_str + ","
		}
		query_str = query_str + ` "` + title + `" ` + column_data_types_slice[i]
		if i == 0 {
			query_str = query_str + " " + "primary key"
		}
	}
	query_str = query_str + " )"
	log.Printf("Query string: `%s`\n", query_str)

	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()

	log.Println("Creating table: " + table_name)
	_, err := db.ExecContext(ctx, query_str)
	if err != nil {
		log.Println("Error when creating product table:")
		log.Fatal(err)
	}
}

// Get the header row and create header placeholder from 2D string slice
func GetHeaderRow(rows [][]string) (string, string) {
	var h_placeholder string
	for i := range rows[0] {
		if i != len(rows[0])-1 {
			h_placeholder = h_placeholder + "$" + fmt.Sprint(i+1) + ", "
		} else {
			h_placeholder = h_placeholder + "$" + fmt.Sprint(i+1)
		}
	}
	header_str := strings.Join(rows[0], ", ")

	return header_str, h_placeholder
}

// Insert rows into the database from 2D string slice, using the first row as headers
func InsertRowsToDatabase(rows [][]string, table_name string, db *sql.DB) {
	header_str, h_placeholder := GetHeaderRow(rows)
	log.Printf("Preparing table '%s' ingest query.\n", table_name)
	prepare_str := "INSERT INTO " + table_name + "( " + header_str + " ) VALUES( " + h_placeholder + " )"
	stmt, err := db.Prepare(prepare_str)
	if err != nil {
		log.Fatal(err)
	}

	for _, row := range rows[1:] {
		// fmt.Println(row)
		row_interface := make([]interface{}, len(row))
		for i, entry := range row {
			row_interface[i] = entry
		}
		_, err := stmt.Exec(row_interface...)
		if err != nil {
			log.Printf("WARN: Error attempting to add row to table '%s': %s\n", table_name, err)
		}
	}
}
